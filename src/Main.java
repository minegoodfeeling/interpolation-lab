import InformationPrinter.MainInformationPrinter;
import Methods.GaussPolynomialMethod;
import Methods.LagrangePolynomialMethod;
import Methods.Method;
import Reader.CaseReader;
import Reader.FunctionToPoints;
import Reader.MyInputOrFileReader;
import org.jfree.data.xy.XYSeriesCollection;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        while (true) {
            MainInformationPrinter informationPrinter = new MainInformationPrinter();
            Scanner scanner = new Scanner(System.in);

            System.out.println(informationPrinter.typeOfInput());
            CaseReader typeOfReader = new CaseReader(scanner, 1, 2);
            final int caseData;
            try {
                caseData = typeOfReader.getCase();
            } catch (Exception e) {
                System.out.println(e.getMessage());
                continue;
            }

            double[] x;
            double[] y;
            double x_1;
            double[] x_f = {-1};
            double[] y_f = {-1};
            if (caseData == 1) System.out.println(informationPrinter.howToInputData());

            MyInputOrFileReader dataReader = new MyInputOrFileReader(scanner, caseData);
            try {
                dataReader.initData();
                x = dataReader.getX();
                y = dataReader.getY();
                x_1 = dataReader.getX_1();
            } catch (Exception e) {
                System.out.println(e.getMessage());
                continue;
            }

            System.out.println(informationPrinter.wantToUseFunction());
            CaseReader wantFunction = new CaseReader(scanner, 1, 2);
            int caseFucntion;
            try {
                caseFucntion = wantFunction.getCase();
            } catch (Exception e) {
                e.getMessage();
                continue;
            }

            if (caseFucntion == 2) {
                FunctionToPoints function = new FunctionToPoints(0, 10, 2, 19);
                try {
                    function.solve();
                } catch (Exception e) {
                    e.getMessage();
                    continue;
                }
                x = function.getX();
                y = function.getY();
            }


            Method lagrangePolynomialMethod = new LagrangePolynomialMethod(x, y, x_1);
            Method gaussPolynomialMethod = new GaussPolynomialMethod(x, y, x_1);

            lagrangePolynomialMethod.solve();
            gaussPolynomialMethod.solve();

            double[][] deltaY = ((GaussPolynomialMethod) gaussPolynomialMethod).getDeltaY();

            System.out.println("Лагранж поспорил: " + lagrangePolynomialMethod.getRoot());
            System.out.println("Гаусс сказал: " + gaussPolynomialMethod.getRoot());

            XYSeriesCollection xyDataSet = new XYSeriesCollection();
            gaussPolynomialMethod.showGraph(xyDataSet);
            lagrangePolynomialMethod.showGraph(xyDataSet);

//            System.out.println("Дельты пошли!");
//            for(int i = 0; i < x.length; i++) {
//                for(int j = 0; j < x.length; j++) {
//                    System.out.print(deltaY[i][j] + "\t");
//                }
//                System.out.println();
//            }

        }


    }
}
