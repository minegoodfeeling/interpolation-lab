package Reader;

public class FunctionToPoints {

    private final double left;
    private final double right;
    private final int typeOfEquation;
    private final int countOfPoints;

    private double[] x;
    private double[] y;

    public FunctionToPoints(double left, double right, int typeOfEquation, int countOfPoints) {
        this.left = left;
        this.right = right;
        this.typeOfEquation = typeOfEquation;
        this.countOfPoints = countOfPoints;
    }

    public void solve() throws Exception {
        x = new double[countOfPoints];
        y = new double[countOfPoints];
        if(left >= right) throw new Exception("Левая граница _правее_ правой");
        if(countOfPoints < 0 || countOfPoints >= 1000) throw new Exception("Вы взяли не то число точек!");

        double step = (right - left) / countOfPoints;

        switch (typeOfEquation) {
            case 1 -> {
                // В качестве функции берём синус
                for(int i = 0; i < countOfPoints; i++) {
                    x[i] = step * i ;
                    y[i] = Math.sin(x[i]);
                }
            }
            case 2 -> {
                // В качестве функции берём косинус
                for(int i = 0; i < countOfPoints; i++) {
                    x[i] = step * i;
                    y[i] = x[i] * x[i];
                }
            }
            default -> {
                // В качестве дефолтной функции берём экспоненту
                for(int i = 0; i < countOfPoints; i++) {
                    x[i] = step * i;
                    y[i] = Math.exp(x[i]);
                }
            }
        }
    }
    public double[] getX() {
        return x;
    }
    public double[] getY() {
        return y;
    }
}
