package Reader;

import java.io.File;
import java.util.Scanner;

public class MyInputOrFileReader{

    private final Scanner scanner;
    private final int caseData;
    private double[] x;
    private double[] y;
    private double x_1;


    public MyInputOrFileReader(Scanner scanner, int caseData) {
        this.scanner = scanner;
        this.caseData = caseData;
    }

    public void initData() throws Exception {
        double[] data = null;
        switch (caseData) {
            case 1 -> {
                int numberOfPoint = 0;
                if (scanner.hasNextInt()) numberOfPoint = scanner.nextInt();
                else throw new Exception("Ввёл не натуральное число!");

                x = new double[numberOfPoint];
                y = new double[numberOfPoint];

                for (int i = 0; i < numberOfPoint; i++) {
                    if (scanner.hasNextLine()) {
                        x[i] = scanner.nextDouble();
                        y[i] = scanner.nextDouble();
                    } else throw new Exception("Вы ввели не числа!");
                }

                System.out.println("Введите значение точки, в которой вы хотите вычислить значение");
                if(scanner.hasNextLine()) {
                    x_1 = scanner.nextDouble();
                }
            }
            case 2 -> {
                scanner.nextLine();
                System.out.println("Введите имя файла");
                String fileName = scanner.nextLine();
                File file = new File("C:\\Users\\MG\\IdeaProjects\\interpolation\\Resourses\\" + fileName);
                Scanner fileScanner = new Scanner(file);

                int numberOfPoint = 0;
                if (fileScanner.hasNextInt()) numberOfPoint = fileScanner.nextInt();
                else throw new Exception("Вы ввели ненатуральное число!");

                x = new double[numberOfPoint];
                y = new double[numberOfPoint];


                for (int i = 0; i < numberOfPoint; i++) {
                    if (fileScanner.hasNextLine()) {
                        x[i] = fileScanner.nextDouble();
                        y[i] = fileScanner.nextDouble();
                    } else throw new Exception("Вы ввели не числа!");
                }

                if(fileScanner.hasNextLine()) {
                    x_1 = fileScanner.nextDouble();
                }
            }
            default -> throw new Exception("Упс! Ошибка с вводом данных");
        }
    }

    public double[] getX(){
        return x;
    }

    public double[] getY() {
        return y;
    }

    public double getX_1(){
        return x_1;
    }
}
