package Methods;

import org.jfree.data.xy.XYSeriesCollection;
import org.knowm.xchart.QuickChart;
import org.knowm.xchart.SwingWrapper;
import org.knowm.xchart.XYChart;

public class LagrangePolynomialMethod implements Method {

    private final double[] x;
    private final double[] y;
    private double answer;
    private final double x_1;

    public LagrangePolynomialMethod(double[] x, double[] y, double x_1) {
        this.x = x;
        this.y = y;
        this.x_1 = x_1;
    }

    @Override
    public void solve() {
        answer = polynomial(x_1);
    }

    @Override
    public double getRoot() {
        return answer;
    }

    private double polynomial(double a) {
        double result = 0;
        int countOfNodes = y.length;

        for (int i = 0; i < countOfNodes; i++) {
            double L = 1;
            for (int j = 0; j < countOfNodes; j++) {
                if (i != j) {
                    L *= (a - x[j]) / (x[i] - x[j]);
                }
            }
            L *= y[i];
            result += L;
        }
        return result;
    }

    @Override
    public void showGraph(XYSeriesCollection xyDataset) {

    }
}
