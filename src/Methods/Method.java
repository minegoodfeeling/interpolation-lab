package Methods;

import org.jfree.data.xy.XYSeriesCollection;

public interface Method {
    void solve();
    double getRoot();
    void showGraph(XYSeriesCollection xyDataset);
}
