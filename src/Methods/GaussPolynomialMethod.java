package Methods;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

import javax.swing.*;
import java.awt.*;

public class GaussPolynomialMethod implements Method {
    private final double[] x;
    private final double[] y;
    private double answer;
    private final double x_1;

    private double[][] deltaY;

    public GaussPolynomialMethod(double[] x, double[] y, double x_1) {
        this.x = x;
        this.y = y;
        this.x_1 = x_1;
    }

    @Override
    public void solve() {
        int numberOfPoints = x.length;
        double t = (x_1 - x[numberOfPoints / 2]) / Math.abs(x[0] - x[1]);
        fillDeltaY();

        if (x_1 > x[numberOfPoints / 2]) {
            answer = polynomialRight(t);
        } else answer = polynomialLeft(t);
    }

    @Override
    public double getRoot() {
        return answer;
    }

    private double polynomialRight(double t) {
        int numberOfPoints = x.length;
        int central = numberOfPoints / 2;
        double result = deltaY[central][0];
        double L = 1;

        for (int i = 0; i < numberOfPoints; i++) {
            if (i % 2 == 0) {
                L *= (t + i / 2) / (i + 1);
            } else L *= (t - i / 2 - 1) / (i + 1);
            if (i + 1 == numberOfPoints) break;
            result += L * deltaY[central - (i + 1) / 2][i + 1];
        }
        return result;
    }

    private double polynomialLeft(double t) {
        int numberOfPoints = x.length;
        int central = numberOfPoints / 2;
        double result = deltaY[central][0];
        double L = 1;

        for (int i = 0; i < numberOfPoints; i++) {
            if (i % 2 == 0) {
                L *= (t - i / 2) / (i + 1);
            } else L *= (t + i / 2 + 1) / (i + 1);
            if (i + 1 == numberOfPoints) break;
            result += L * deltaY[central - (i + 2) / 2][i + 1];
        }
        return result;
    }

    private double[][] fillDeltaY() {
        int numberOfPoints = x.length;
        deltaY = new double[numberOfPoints][numberOfPoints];

        for (int i = 0; i < x.length; i++) {
            for (int j = 0; j < x.length - i; j++) {
                if (i == 0) {
                    deltaY[j][i] = y[j];
                } else {
                    deltaY[j][i] = deltaY[j + 1][i - 1] - deltaY[j][i - 1];
                }
            }
        }
        return deltaY;
    }

    private double maxX() {
        double max = Double.MIN_VALUE;
        for (double v : x) {
            if (v > max) max = v;
        }
        return max;
    }

    private double minX() {
        double min = Double.MIN_VALUE;
        for (double v : x) {
            if (v < min) min = v;
        }
        return min;
    }

    public double[][] getDeltaY() {
        return deltaY;
    }

    @Override
    public void showGraph(XYSeriesCollection xyDataset) {
        XYSeries series1 = new XYSeries("Многочлен Гаусса");
        XYSeries series2 = new XYSeries("Точки интерполяции");
        XYSeries series3 = new XYSeries("Искомая точка");
        double x0 = minX();
        double xn = maxX();
        double h = Math.abs(x[0] - x[1]);

        for(float i = (float) ((float) x0 - 0.01); i < xn + 0.01; i+=(xn - x0)/1000){
            double t = (i - x[x.length / 2]) / h;
            series1.add(i, polynomialLeft(t));
        }

        for(int i = 0; i < x.length; i++) {
            series2.add(x[i], y[i]);
        }

        {
            series3.add(x_1, answer);
        }

        xyDataset.addSeries(series3);
        xyDataset.addSeries(series2);
        xyDataset.addSeries(series1);
        JFreeChart chart = ChartFactory
                .createXYLineChart("Интерполяционный многочлен Гаусса", "x", "y",
                        xyDataset,
                        PlotOrientation.VERTICAL,
                        true, true, true);
        XYPlot plot = chart.getXYPlot();

        XYLineAndShapeRenderer renderer = new XYLineAndShapeRenderer();
        renderer.setSeriesPaint(0, Color.BLACK);
        renderer.setSeriesPaint(1, Color.white);
        renderer.setSeriesPaint(2, Color.RED);
        renderer.setSeriesLinesVisible(1, false);
        renderer.setSeriesShapesVisible(2, false);
        renderer.setSeriesStroke(2, new BasicStroke(1.0f));
        plot.setRenderer(renderer);

        JFrame frame =
                new JFrame("Интерполяция");
        // Помещаем график на фрейм
        frame.getContentPane()
                .add(new ChartPanel(chart));

        frame.setSize(800,800);
        frame.show();
    }
}
